# TB data analysis: configuration files for EUTelescope 
 
EUTelescope configuration files for the test beam data produced during June-2018
at SPS, CERN.

Remember to create: `output/database`, `output/histograms`,
`output/lcio` and `output/logs` in the output subfolder directory

## Prerequisites 
 * EUDAQ Docker container: https://github.com/duartej/dockerfiles-eudaqv1/tree/eutelescope
 * Eudaq installation: https://github.com/duartej/eudaq
   * Remove the `$HOME/repos/eudaq` installed by the dockerfiles-eudaqv1 package, and
     substitute by [duartej/eudaq](https://github.com/duartej/eudaq)
   * Use branch [v1.7-dev](https://github.com/duartej/eudaq/tree/v1.7-dev) for run <= 270
   * Use branch [multiple_producers](https://github.com/duartej/eudaq/tree/multiple_producers) for run > 270
 * EUTelescope already installed in the EUDAQ docker container.
   * Processors algorithms, available paremeters, could be checked at
     https://github.com/eutelescope/eutelescope/processors (the actual code in the EUDAQ container
     could slightly change due to the adaptation to the new RD53A data)


## Content
 * **config.cfg**: Configuration parameters for the `jobsub` script
 * **runlist.csv**: Identification of run with geometry file (and other parameters)
 * **gear**: folder containing the geometry description of the setup, telescope,
         REF and DUTs planes
 * **steering-templates**: foolder containing the steering files to be used as 
         templates for the `jobsub` script. Any parameter of the form 
         `@name@` must exist either in the `config.cfg` file or as a `jobsub`
         option

